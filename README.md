# WEBCASE simple builder 

[![N|Solid](https://webcase.studio/wp-content/themes/webcase/build/static/images/logo3.svg)](https://webcase.studio/)
## New project creation

For that you'll need a [cookiecutter](https://github.com/audreyr/cookiecutter).

After instalation simply run:

```bash
cookiecutter https://gitlab.com/web_case/webcase-simple-builder
```

And answer the simple questions, that script will ask you for.

That's it, project scaffolding is ready.